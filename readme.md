﻿# Logger - a logger implementation for Windows

 Wolfgang Graf, xing@wogra.net, 2017, BSD license

| *logger* command is a utility available in most unix - like systems.

| in Windows you do not have a similar functionality. This is a .NET reimplemenation


## Features

Ability to log information via command line to various destinations including

- Logfile

- Databases

- Remote syslog facilities

- Stackable

- Eventlog

  

- Easy to use

- Configurable

- Extendable

- usable in piped commands 

- usable in cmd,bash and powershell environments


## Sample usage

Logger comes with a default configuration on board, if this is not suitable for you see 
configuration section.

Open command line, go to the Logger.exe directory

Note that in different environments it makes a difference if you write

- Logger.exe,

- logger.exe,

- logger (which might then use something already in your path) or

- Logger

### Direct logging

| command              | Explanation                                                  |
| -------------------- | ------------------------------------------------------------ |
| Logger asdf          | will print to default.log file and console with default severity info |
| Logger -l ERROR asdf | will print to default.log file and console with severity error |
|                      |                                                              |



### Logging from stdin

This one is usable in batch mode where you want to log the output of one command.

Per default stdout is reported with the severity level you use via the -l parameter and

stderr is always reported as severity error.

This works as long errors and output are outputted from your program.

Please note that output redirection works slightly different in every environment. 

| command                             | Explanation                                                  |
| ----------------------------------- | ------------------------------------------------------------ |
| echo asdf > Logger -l info  -  2>1& | will send asdf as info message. The key sign to read stdin is either "-" or an empty message |
| logger -                            |                                                              |
|                                     |                                                              |

### Use syslog

Example for configuration can be found at : https://tewarid.github.io/2018/06/01/logging-to-syslog-using-nlog.html
 

| command                           | Explanation                                                  |
| --------------------------------- | ------------------------------------------------------------ |
| logger -c syslog.sample.conf asdf | will use the sample syslog configuration. See https://github.com/luigiberrettini/NLog.Targets.Syslog |
|                                   |                                                              |
|                                   |                                                              |





## Requirements

.NET framework 4.5+ installed OR Mono installed


## Building from source

A Visual Studio 2019 project is included to build. You do not need to have VS installed, 
msbuild.exe or xbuild is ok




