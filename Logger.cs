﻿using NLog;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{

    /**
     * Implementation .NET for the Unix logger command.
     * @2017 xing@wogra.net Wolfgang Graf  MIT license
     * See readme.md for usages
     * **/

    public class Logger
    {

        bool inited = false;
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private string customConfig = null;
        static RootCommand rootCommand = new RootCommand
            {
                new Argument<string>("message", "the message to log, - for input from stdin"),
                new Option<string>(
                    aliases: new string[]{ "-p" },
                    getDefaultValue: ()=> "level.notice",
                    description : "Only valid when using a syslog facilty. Enter the message with the specified priority. The priority may be specified numerically or as a ‘‘facility.level’’ pair. For example, ‘‘-p local3.info’’ logs the message(s) as info rmational level in the local3 facility. The default is ‘‘user.notice.’’"),
                new Option<string>(
                    aliases: new string[] { "-l", "--level" },
                    getDefaultValue: () => LogLevel.Info.ToString(),
                    description: string.Concat("Only valid if using a nlog facility. Level to log as, default Info, available levels:", getLogLevels())
                    ),
                new Option<string>(
                    aliases: new string[] { "-c", "--configuration" },
                    description: string.Concat("Override configuration file used, defaults to app.config, nlog.config in current dir, see https://github.com/NLog/NLog/wiki/Configuration-file#configuration-file-locations" )
                    )
            };
        public static int Main(string[] args)
        {
            Logger instance = new Logger();
            rootCommand.Description = "Linux logger reimplementation for windows, Wolfgang Graf, xing@wogra.net CL 2017, BSD license";
            try
            {
                rootCommand.Handler = CommandHandler.Create<string, string, string, string>((message, priority, level , configuration) =>
               {
                   if (configuration != null)
                       instance.customConfig = configuration;
                   instance.logit(priority, level, message);
               });
                // Parse the incoming args and invoke the handler
                return rootCommand.InvokeAsync(args).Result;
            }
            catch (Exception e)
            { instance.log.Error(e.Message); return -1; }
        }


        /// <summary>
        /// log on or more lines into log handler
        /// </summary>
        /// <param name="priority">only valid when using the syslog facility</param>
        /// <param name="level">Nlog configured loglevel</param>
        /// <param name="message">the message to log, if "-" the stdin for the command is used</param>
        public void logit(string priority, string level, string message)
        {
            initLog();
            Console.WriteLine("Message:" + message + "*");
            LogLevel l = LogLevel.FromString(level);
            if (l == null)
                l = LogLevel.Info;

            if (message == null || message.Equals("-"))
            {
                Console.WriteLine("get sdtin");
                using (TextReader instrm = new StreamReader(Console.OpenStandardInput()))
                using (TextReader errstrm = new StreamReader(Console.OpenStandardError()))
                {
                    string li = null;
                    string er = null;
                    do
                    {
                        li = instrm.ReadLine();
                        if (li != null && li.Length > 0)
                            log.Log(l, li);
                        // hopefully it is correct to log error stream entries as error hardcoded?
                        er = errstrm.ReadLine();
                        if (er != null && er.Length > 0)
                            log.Error(er);
                    }
                    while ((li != null && li.Length > 0) && (er != null && er.Length > 0));
                }
            }
            else
            {
                                log.Log(l, message);
            }
            LogManager.Flush();
        }
        /// <summary>
        /// reentrant init of the log handler with default functionality
        /// </summary>
        private void initLog()
        {
            if (!inited)
            {
                if (customConfig != null)
                {
                    LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(customConfig);
                  
                }
                // default config files for nlog
                else if (!File.Exists("nlog.config") && (!File.Exists("app.config")))
                {
                    /// using a default configuration
                    var config = new NLog.Config.LoggingConfiguration();
                    // Targets where to log to: File and Console
                    var logfile = new NLog.Targets.FileTarget("logfile") { FileName = string.Concat("./default.log") };
                    var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
                    // Rules for mapping loggers to targets            
                    config.AddRule(LogLevel.Debug, LogLevel.Error, logconsole);
                    config.AddRule(LogLevel.Debug, LogLevel.Error, logfile);

                    // Apply config           
                    NLog.LogManager.Configuration = config;
                }
                // for stdin
                inited = true;
            }
        }
        /// <summary>
        /// for help display
        /// </summary>
        /// <returns>configured loglevels</returns>
        private static string getLogLevels()
        {
            string sb = "";
            LogLevel.AllLevels.ToList().ForEach((d) => { sb = string.Concat(sb, d, " "); });
            return sb;
        }
    }
}
